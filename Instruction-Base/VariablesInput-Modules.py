# Simone Tugnetti

# import random  # Richiama l'intera libreria random senza alcun alias
import random as randomlibrary  # Utilizzo della librearia random usando uno pseudonimo
from random import randrange, random  # Richiamo le singole funzioni della libreria

# Variables
message = "Funziona?"
value = 1
anotherValue = 0

print(message)
print("il numero è", value, "mentre l'altro è", anotherValue)

# Update the variables
value = 2
anotherValue = 3
print(value + anotherValue)

# Increments and Decrements
value += 1
anotherValue -= 1
print(value + anotherValue)


# Input -> String
inputVal = input("Inserisci il tuo nome: ")
print("Il tuo nome", inputVal)

# Conversione Input
a = input("Inserisci il primo valore: ")
b = input("Inserisci il secondo valore: ")
print("Somma", int(a) + int(b))


# Using Module
# prob = random.random()
prob = random()
prob2 = randomlibrary.random()
print(prob)
print(prob2)

# dice = random.randrange(1, 7)
dice = randrange(1, 7)
dice2 = randomlibrary.randrange(1, 7)
print(dice)
print(dice2)
