# Simone Tugnetti
import math


# Defination of a function
def square(x):
    return x * x


def sub(x, y):
    return x - y


# Call Functions
print(math.pow(3, 3))
print(math.sqrt(4))
print(square(4))
print(sub(4, 1))
print(len("Hello"))

# Python è sequenziale, perciò il calcolo dell'operazione va visto da sinistra verso destra per poi calcolarne i
# risultati ottenuti
a = 3
b = 2
print(square(a + sub(square(a), 2 * b)))


# Data Types
print(type("Hello"))
print(type(100))
print(type(12.0))

# Stringhe
print("Hello world!")  # Stringa su singola riga

# Stringa su più righe con l'ausilio di apostrifi o doppi apici
print("""Hello World, but this isn't a 
normal "string" """)

# Stampa più valori
print(10, 14, 15, 98)
print(22, "hello", 1234)

# Type Conversion Functions
print("10", int("10"))
print(30, type(str(30)))
print(40, float(40))
print(-4.5, float(-4.5))
print(2.5, int(2.5))
