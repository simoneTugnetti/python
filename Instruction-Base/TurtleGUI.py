# Simone Tugnetti
from turtle import Screen, Turtle

# Turtle Program
wn = Screen()
wn.bgcolor("lightgreen")

alex = Turtle()
alex.color("white")
alex.pensize(5)

tess = Turtle()
tess.pensize(5)

alex.forward(150)
alex.left(90)
alex.forward(75)
alex.left(90)
alex.forward(150)
alex.left(90)
alex.forward(75)

tess.right(45)
tess.forward(150)
tess.right(180-45)
tess.forward(215)
tess.right(90+45)
tess.forward(150)

alex.clear()
tess.clear()

elan = Turtle()
elan.pensize(5)
elan.color("green")
distance = 50
angle = 90

for _ in range(15):
    elan.forward(distance)
    elan.right(angle)
    distance += 10
    angle -= 3

elan.clear()

task = Turtle()
task.color("blue")
task.pensize(5)
task.shape("turtle")

dist = 5
task.up()  # Non lascia una linea visibile

for _ in range(30):
    task.stamp()  # Stampa la forma della tartaruga a schermo
    task.forward(dist)
    task.right(24)
    dist += 2

wn.exitonclick()
