# Simone Tugnetti

# Utilizzo base di diversi tipi

# Stringa
print("Hello World")

# Intero
val = 100
print(val)

# Float
print(3.14)


# Operatori
# Python possiede, oltre ai normali operatori anche alcuni per avere risultati determinati

print(100 / 3)  # Darà il resto

print(100 // 3)  # NON darà il resto, arrotonda per difetto

print(10.0 // 3)  # Restituirà un Float

print(10 % 3)  # Modulo

# Elevazione a potenza
print(4 * 2)
print(4 ** 2)


# Ordine di Operazioni
# L'ordine di esecuzione delle operazioni è lo stesso delle normali leggi matematiche

print(10 + 2 * 3)  # Prima la moltiplicazione

print(10 * 2 ** 2)  # Prima l'elevazione

print((10 * 2) ** 2)  # Prima le parentesi
