# Simone Tugnetti


# Strings
s = "   Hello, world   "  # Basic String

# Multiple Line String with ' or " in it
s2 = """Hello world!
How are you?
You'll be fine"""

print(s2.count("o"))  # Conterà il numero di 'o' presenti nella stringa
print(s2.index("are"))  # Restituirà l'indice della prima apparizione dell'elemento da ricercare

print(s2.split())  # Restituirà una lista contenente la stringa splittata osservando gli spazi all'interno
print(s2.split("o"))  # Splitterà la stringa ogni volta che viene riconosciuto l'argomento passato

# Utilizzando Join si possono unire gli elementi all'interno di una lista per creare una nuova stringa
# utilizzando un separatore definito
print(" ".join(["Mi", "chiamo", "Simone"]))

# Example of type class
string = '5'
num = 5

print(type(string))
print(type(num))
print(float(string) + num)

# Non-Mutable Methods
print(s.upper())  # Converte la stringa in uppercase

t = s.lower()  # Converte la stringa in lowercase
print(t)

print(s.count("o"))  # Conta il numero di caratteri richiesti

print(s.strip())  # Restituisce la stringa senza gli spazi all'inizio e alla fine della stessa

print(s.replace("o", "f"))  # Rimpazza il primo elemento con il secondo dato

# Utilizzando le parentesi graffe all'interno di una stringa, è possibile inserire successivamente
# informazioni nelle rispettive posizioni tramite l'ausilio del metodo format()
form = "Ciao {}, benvenuto in questo {}".format("Simone", "documento")
print(form)

i = 7.1234567

print("""Un decimale: {:.1f}
Due decimali: {:.2f}
Quattro decimali: {:.4f}""".format(i, i, i))

print(s[::-1])  # Inverte l'intera stringa

# --------------------

print()

# Lists
a = "5 hundred"

myList = [3, 1, 2, 5]
myList2 = [a, 44]

print(myList, len(myList))
print(myList2, len(myList2))
print(myList[1])
print(myList + myList2)  # Crea una nuova lista concatenando le due create in precedenza
print(myList * 3)  # Restituirà tre volte la stessa lista

print(a[2])  # Questo è possibile dato che le stringhe sono liste di caratteri

# Questo è possibile dato che l'indicizzazione degli array può essere vista anche al contrario con numeri negativi
print(a[len(a)-1], a[-1])

print()

# Mutating Methods in List
myList.append(4)  # Inserisce alla fine della lista un elemento
print(myList)

myList.insert(1, 1.5)  # Inserisce un elemento ad un determinato indice scalando la lista di conseguenza
print(myList)

print(myList.count(2))  # Conta il numero di elementi avente lo stesso argomento dato

print(myList.index(3))  # Restituisce la posizione dell'elemento richiesto

myList.reverse()  # Inverte l'ordine della lista
print(myList)

myList.sort()  # Riordina la lista in maniera crescente
print(myList)

myList.remove(1.5)  # Rimuove l'elemento desiderato
print(myList)

lastItem = myList.pop()  # Rimuove e restituisce l'ultimo elemento della lista
print(myList, lastItem)


# --------------------------

print()

# Tuples -> Liste Immutabili
myTuple = ('tuple', 'one', 2, 'three', "tuple")
myTuple2 = ()

print(type(myList))
print(type(myTuple))
print(type(myTuple2))

# Count viene utilizzato per conteggiare quante volte un determinato elemento appare all'interno di un oggetto
print(myTuple.count("tuple"))
print(myTuple.count(2))
print(myTuple.count("e"))  # Non conteggia il numero di lettere ma se esiste l'elemento avente nome "e"

print(myTuple.index("three"))  # Restituirà il primo indice avente quell'elemento nella tupla


# The Slice Operator -> Ritorna la lista di oggetti in un range specificato
myTuple3 = ("one", 2, "three", 4, "five", 6, "seven", 8, "nine", 10)

print(myTuple3[3])

print(myTuple3[4:-1])  # Stampa i valori dalla posizione 4 fino alla penultima posizione
print(myTuple3[4:])  # Stampa dalla posizione 4 fino all'ultima posizione
print(myTuple3[:5])  # Stampa dalla posizione 0 fino alla posizione 4

print(a[:5])  # Restituisce comunque una stringa
