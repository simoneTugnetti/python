# Simone Tugnetti


# Mutation
fruit = ["banana", "orange", "apple"]
print(fruit)

fruit[0] = "pineapple"
fruit[-1] = "strowberry"
print(fruit)

print()

alist = ['a', 'b', 'c', 'd', 'e']
print(alist)

del alist[1]  # Elimina l'elemento selezionato
print(alist)

alist[1:3] = ['x', 'y']
print(alist)

del alist[2:]
print(alist)

print()

s = "Hello World"
print(s)

news = "Maybe " + s[0:]
print(news)

print()

a = "banana"
b = "banana"

print(a is b)  # Identifica se un oggetto è identico ad un altro

lista = [80, 81, 82]
listb = [80, 81, 82]

print(lista is listb)  # Restituirà False, dato che gli indirizzi delle liste sono differenti e quindi oggetti diversi

print(lista == listb)  # Restituirà True, dato che i valori all'interno delle liste sono equivalenti

print(id(lista))  # Identificativo dell'oggetto
print(id(listb))

print()

# Aliasing
print(lista is listb)

lista = listb  # In questo modo le due liste vengono rese identiche
print(lista is listb)
print(lista == listb)

listb[0] = 55  # Modificando listb, verrà anche modificato lista, dato che entrambi puntano allo stesso indirizzo
print(lista)

lista += [99]  # Utilizzando += viene modificato il valore anche a livello di indirizzi
listb = listb + [100]  # Con questa espressione, invece, viene modificata solo la lista desiderata
print(lista)
print(listb)

print()

# Cloning
newListA = [1, 2, 3, 4, 5]

# In questo modo verranno clonati gli elementi all'interno di newListA ma rendendo entrambi indipendenti tra loro
newListB = newListA[:]

print(newListA)
print(newListB)

print(newListA is newListB)
print(newListA == newListB)
