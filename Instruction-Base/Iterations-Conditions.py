# Simone Tugnetti


# Iteration

print("prima riga")

# Con valore
for i in range(3):
    print("riga stampata", (i+1), "volte")

# Senza valore
for _ in range(2):
    print("Stampa riga senza valore")

print(list(range(0, 10)))  # Casting List of range of value

print()

# Vi è anche la possibilità di visualizzare gli elementi effettivi all'interno di una lista
for name in ["Gianni", "Pinotto"]:
    print("Ciao ", name)

print()

for character in "Hi, my name is Simone"[-6:]:
    print(character)


# Accumulator Pattern
a = 0

for i in [2, 4, 6, 8]:
    a += i

print(a)

str1 = "I like nonsense, it wakes up the brain cells. Fantasy is a necessary ingredient in living."

numbs = 0
for _ in list(str1):
    numbs += 1

print(numbs)


# For in cascata
for i in range(5):
    for j in range(3):
        print(i, j)


print()

# ------------------------------------


# Wile Loops
a = 10
b = 0
c = 0
while a > b:
    b += 1
    c += 3

print(c)

# Listener Loop -> Chiedo all'utente un valore all'interno di un Loop
newSum = 0
x = -1
while x != 0:
    x = int(input("Inserisci un valore: "))
    newSum += x

print(newSum)

# Infinite Loop and Break
while True:
    print("Questo verrà stampato una sola volta")
    break

# Continue -> ripete dalla condizione del loop
# Break -> Termina il loop
y = 3
while y <= 10:
    if y <= 5:
        y += 1
        continue
    if y > 5:
        print("y maggiore di 5")
        break


# ------------------------------------

print()

# Conditionals
print(True, False)
print(type(True), type(False))

value = 6

print(5 == 5)
print(5 == 6)
print(5 == '5')
print(5 < 6)
print(7 >= 6)
print(5 != 1)
print(0 < value < 10)

print()

# Logical Operator
x = 30

print(x > 0 and x != 30)  # Entrambi devono essere True per restituire True

print(x <= 50 or x > 66)  # Almeno uno deve essere True per restituire True

print(not x == 30)  # Se il risultato dell'espressione risulta True, restituirà False

print()

# in -> Verifica se l'elemento a sinistra esiste nell'elemento a destra
print('a' in 'apple')
print('b' in 'basics')
print('Gianni' in ['Gianni', 'Burnes'])


# Conditionals
y = 10

if x % 2 == 0:
    print(x, "risulta divisibile per 2")
else:
    print(x, "risulta NON divisibile per 2")


if y == 10:
    print(y, "e' uguale a 10")
else:
    print(y, "non e' uguale a 10")


courses = ["ENGR 101", "SI 110", "ENG 125", "SI 106", "CHEM 130"]

if "SI 106" in courses:
    output = "You can apply to SI!"
else:
    output = "Take SI 106!"


# Nested Conditional
if x == y:
    print(x, "e' uguale a", y)
else:
    if x > y:
        print(x, "e' maggiore di", y)
    else:
        print(x, "e' minore di", y)


# Chained Conditional
if x == y:
    print(x, "e' uguale a", y)
elif x > y:
    print(x, "e' maggiore di", y)
else:
    print(x, "e' minore di", y)

