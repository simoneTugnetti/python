# Simone Tugnetti


# Utilizzo dei Files


# Lettura -> Viene aperto il file con il nome nel primo argomento e l'azione da eseguire nel secondo
fileRef = open("name.txt", "r")

anotherFile = open("../myData/name.txt", "r")  # Apre un file usando un relative Path

contents = fileRef.read()  # Vengono letti i caratteri
lines = fileRef.readlines()  # Vengono lette le righe

print(len(lines))  # Conta il numero di righe all'interno del file

print(contents[:20])  # Vengono stampati i primi venti caratteri
print(lines[:3])  # Vengono stampate le prime tre righe in una singola riga

# Verranno stampate tre righe, una per ogni riga
for line in lines[:3]:
    print(line)  # Con una riga di separazione tra loro

for line in lines[:3]:
    print(line.strip())  # Senza una riga di separazione tra loro

for line in fileRef:
    print(line)  # Stampa tutte le righe all'interno del file

fileRef.close()  # Viene chiuso il file

# L'equivalente di newFile = open("name.txt", "r") senza l'obbligo di dover utilizzare newFile.close()
with open("name.txt", "r") as newFile:
    for line in newFile:
        print(line)


# ---------------------------------


# Scrittura
fileWrite = open("writeFile.txt", "w")

# Scrive il range da 0 a 50 in un unica riga senza spazi
for number in range(50):
    fileWrite.write(str(number) + '\n')  # Aggingendo \n verrà applicata la nuova riga

fileWrite.close()

fileRead = open("writeFile.txt", "r")

print(fileRead.read()[:4])  # Ottiene i primi 4 caratteri e li stampa

fileRead.close()

with open("newWriteFile.txt", "w") as newWriteFile:
    for number in range(20):
        newWriteFile.write(str(number) + "\n")


# ---------------------------------


# CSV Output -> Il CSV è un formato facilmente leggibile tramite Excel che comprende righe e colonne


# Read
fileConn = open("fileCSV.txt", "r")

# Un file CSV può comprendere ',' per delimitare gli elementi in una riga per poi crearne una nuova
lines = fileConn.readlines()
header = lines[0]  # Un file CSV comprende un header dove indicare il tipo di dato
line = header.strip().split(',')  # Possono quindi venire letti come singoli elementi in una lista
print(line)

# Stampando poi gli elementi inerenti al proprio header
for row in lines[1:]:
    vals = row.strip().split(',')
    print("{}: {}; {}".format(vals[0], vals[1], vals[2]))

fileConn.close()


# Write
persons = [("Giovanni", 18, "Torino"), ("Gianni", 33, "Napoli"), ("Marco", 12, "Milano")]
header = "Nome,Eta,Luogo"

outputCSV = open("writeFile.csv", "w")
outputCSV.write(header)  # Scrivo l'header del file
outputCSV.write("\n")

# Scrivo i dati nella giusta posizione in base all'header
for person in persons:
    rows = "{},{},{}".format(person[0], person[1], person[2])
    outputCSV.write(rows)
    outputCSV.write("\n")

outputCSV.close()
