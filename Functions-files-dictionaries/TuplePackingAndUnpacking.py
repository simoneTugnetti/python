# Simone Tugnetti


# Tuple Packing and Unpacking

# Crea una tupla di elementi quando vengono ritornati più valori
def sum_two_different_value(x, y):
    """Return sum of two value in tuple"""
    w = x + 5
    z = y + 10
    return w, z


def sum_two_value(x, y):
    w = x + y
    return w


# Tuple Packing
tupla = "Giovanni", 12, 32.5, False
print(sum_two_different_value(12, 15))


# Tuple Unpacking
a, b, c = 2, "Paolo", True

# E' possibile passare lo stesso numero di valori all'interno di una funzione tramite tupla
z = (7, 6)
print(sum_two_value(*z))

dictionary = {"key1": 12, "key2": 33, "key5": 66}

lista = [1, 2]

for key, value in dictionary.items():
    print(key, "value:", value)
