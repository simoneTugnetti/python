# Simone Tugnetti


# Dictionaries -> Liste che possiedono keyword al posto degli indici

engToIta = {}

engToIta['one'] = 'uno'
engToIta['two'] = 'due'
engToIta['three'] = 'tre'

print(engToIta)

newEngToIta = {'one': 'uno', 'two': 'due', 'three': 'tre'}

print(newEngToIta)
print(newEngToIta['two'])

print()


# -------------------------


# Operations in Dictionaries

del engToIta['two']  # Deleting item

print(engToIta)

newEngToIta['two'] = 2  # Reassign Item

print(newEngToIta)

# Create an item and reference to another item
newEngToIta['four'] = newEngToIta['two'] * 2

print(newEngToIta)

print(len(engToIta))  # Length of a dictionary

print()


# --------------------------


# Methods of Dictionary

# Restituisce una pseudo lista delle key all'interno di un dictionary
for key in newEngToIta.keys():
    print(key, "e' la chiave con valore", newEngToIta[key])

# In automatico, viene intesa l'iterazione tramite le key
for key in newEngToIta:
    print("Key: ", key)

print(list(newEngToIta.keys()))  # Lista delle key
print(list(newEngToIta.values()))  # Lista dei valori
print(list(newEngToIta.items()))  # Lista di tuple contenenti key e valore

# Restituisce true se la key esiste all'interno del dictionary
print('five' in newEngToIta)
print('four' in newEngToIta)

print(newEngToIta.get('one'))  # Equivalente a newEngToIta['one']
print(newEngToIta.get('five'))  # Restituisce None invece di Error nel caso di newEngToIta['five']

# Nel caso in cui non esista la key, restituirà il valore presente nel secondo argomento
print(newEngToIta.get('five', 0))

print()

# -------------------------------

# Aliasing and Copying

person = {'Gianni': 'uomo', 'Francesca': 'Donna'}
alias = person

print(alias is person)  # Se alias punta allo stesso oggetto di person

# Puntando allo stesso oggetto, le modifiche verranno apportate ad entrambi
alias['Francesca'] = 'altro'
print(person['Francesca'])

print()


# ----------------------


# Accumulating

numbers = {'five': 5, 'eight': 8, 'eleven': 11}
c = 'eight'

if c == 'eight':
    numbers[c] = numbers[c] + 8
elif c == 'five':
    numbers['five'] = numbers[c] + 5

carCount = {}
word = 'pasta'

# Nel caso la lettera non esista all'interno del dictionary, verrà prima inizializzata per poi essere incrementata
for car in word:
    if car not in carCount:
        carCount[car] = 0

    carCount[car] += 1

print(numbers)
print(carCount)
