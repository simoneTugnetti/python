# Simone Tugnetti


# Sorting -> Ordinamento

def double(x):
    return x * 2


list1 = [1, 6, -2, 5]
list2 = ['Blueberry', 'Pineapple', 'Apple']

list1.sort()
print(list1)

list2.sort()
print(list2)

list3 = [7, -5, 10, 1]

list4 = sorted(list3)
print(list4)

print(sorted(list3))  # Questa funzione non si applica direttamente all'oggetto
print(list3)

print(sorted(list3, reverse=True))  # Inverte l'ordinamento

print(sorted(list3, key=double))  # Richiama la funzione tramite key e esegue il sorting con nuovi valori

print(sorted(list3, key=lambda x: double(x)))  # Richiama direttamente il valore da passare alla funzione

d = {'three': 3, 'two': 5, 'one': 1}

# Sorting di un dictionary tramite valore
for x in sorted(d.keys(), key=lambda k: d[k]):
    print(x, "value:", d[x])

tups = [('B', 15, 22), ('C', 1, 98), ('D', 13, 90), ('A', 34, 8)]

# Le tuple vengono ordinate in base al primo valore
for tup in sorted(tups):
    print(tup)

# Breaking Ties
listChar = ['bbbbbbbbbb', 'aaaa', 'dddddddddddddd', 'cc']

# Crea una tupla per eseguire il sorting in base al primo valore, in questo caso la lunghessa della stringa
for x in sorted(listChar, key=lambda char: (len(char), char)):
    print(x)
