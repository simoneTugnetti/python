# Simone Tugnetti


# Functions

# Funzione che stampa a video "Hello World"
def hello():
    print("Hello World")


# Creazione di una funzione con parametri
def hello_name(name, age):
    print("Hello", name, "you have", age, "years")


# Creazione di una funzione avente un valore di ritorno
def double(x):
    y = x * 2
    return y


# La variabile "w" è chiamata variabile locale, mentre la variabile "temp" è definita variabile globale
# La keyword global è opzionale, identifica quale variabile all'interno della funzione è globale
def local_and_global(x):
    global temp
    w = temp + 2
    w += x
    return w


# Una funzione può anche essere richiamata all'interno di un'altra funzione
def sum_of_double(x, y, z):
    return double(x) + double(y) + double(z)


# Le variabili sono locali all'interno di una funzione, ma gli oggetti no
# Dalla variabile valf viene passato semplicemente un valore di riferimento
# Dalla lista listf viene passato l'indirizzo di un intero oggetto modificabile, ad esempio una lista
def mutable_variable(valf, listf):
    valf = 2 * valf
    listf[1] = "Burcalo"


# Optional Parameter -> Può essere omesso il passaggio di parametro per un parametro della funzione
def append_list(a, new_list=[]):
    new_list.append(a)
    return new_list


# E' possibile assegnare dei valori di default ai parametri di una funzione, anche tramite keyword
initial = 6


def value_in_parameter(x, y=5, z=initial):
    print(x, y, z)


hello()
hello_name("Gianni", 12)

num = 4
print("Il quadrato di", num, "e'", double(num))

temp = 5
print(local_and_global(num))

print("Somma dei quadrati", sum_of_double(2, 3, 4))

val = 2
newList = ["Gianni", "Pinotto"]
mutable_variable(val, newList)
print(val)
print(newList)

value = 2
print(append_list(value))
print(append_list(value, ["Giovanni"]))

value_in_parameter(2)
value_in_parameter(4, 6)


# Function Lambda Expression

def f(x):
    return x - 1


newF = lambda x: x-1

print(f(4))
print(newF(4))
