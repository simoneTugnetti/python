# Simone Tugnetti


# Classes

class Point:

    # Variabile locale
    z = 10

    # Costruttore -> Metodo opzionale utilizzato per creare un'istanza passando dei valori iniziali
    def __init__(self, x, y):
        self.x = x
        self.y = y

    # Metodo String -> Alla chiamata dell'istanza della classe in stampa, essa stamperà una stringa personalizzata
    def __str__(self):
        return 'Variabile x: {}, Variabile Y: {}, Variabile Z: {}'.format(self.x, self.y, self.z)

    # Metodi di Aggregazione -> Utilizzato per unire due oggetti insieme ritornando un unico oggeto
    # Viene richiamato quando si esegue un'operazione di addizione tra due oggetti
    def __add__(self, other_point):
        return Point(self.x + other_point.x, self.y + other_point.y)

    # Viene richiamato quando si esegue un'operazione di sottrazione tra due oggetti
    def __sub__(self, other_point):
        return Point(self.x - other_point.x, self.y - other_point.y)

    # E' possibile creare funzioni all'inerno della classe, chiamati metodi
    def get_x(self):
        return self.x  # Self -> Identifica l'oggetto stesso

    def get_y(self):
        return self.y

    def sorted_y(self):
        return self.y

    # pass  # Identifica una classe vuota


# Creazione di un'istanza della classe Point, le variabili ora sono oggetti che puntano alla suddetta classe
point1 = Point(12, 98)
point2 = Point(11, 16)

print(point1)
print(point2)

print(point1.get_x(), point2.get_x())

# E' possibile creare variabili all'interno della classe anche se non sono dichiarate al suo interno
point1.x = 5
point2.x = 10

print(point1.get_x(), point1.get_y())
print(point2.get_x(), point2.get_y())

print(point1 + point2)
print(point1 - point2)

list1 = [point1, point2]

print()

for f in list1:
    print(f)

print()

# Per eseguire l'ordinamento di una lista di oggetti, è possibile prelevarne un singolo dato ed eseguire l'ordinamento
# tramite quello stesso dato
for f in sorted(list1, key=Point.sorted_y):
    print(f)
