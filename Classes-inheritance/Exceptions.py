# Simone Tugnetti


# Exceptions -> Nel caso in cui un'operazione non va a buon fine, scatta un'eccezione che informa del fallimento

items = [1, 2]
second = items[1]

# Entra in except solo se il blocco di codice all'interno di try non va a buon fine
# E' possibile anche utilizzare più except per lo stesso try
try:
    x = 10 / 0
    third = items[2]
except IndexError:
    pass
except ZeroDivisionError:
    print('Impossibile Dividere per 0')
