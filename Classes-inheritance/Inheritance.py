# Simone Tugnetti


# Inheritance -> Ereditarietà tra classi

CURRENT_YEAR = 2020


# Superclasse -> Classe base, informazioni e metodi generici di ogni persona
class Person:
    def __init__(self, name, year_born):
        self.name = name
        self.year_born = year_born

    def get_age(self):
        return CURRENT_YEAR - self.year_born

    def __str__(self):
        return "{}: {}".format(self.name, self.get_age())


# Sottoclasse -> Classe che eredita variabili e metodi dalla superclasse
class Student(Person):
    def __init__(self, name, year_born, knowledge):
        Person.__init__(self, name, year_born)
        self.knowledge = knowledge

    def study(self):
        self.knowledge += 1

    def __str__(self):
        return Person.__str__(self) + ', {}'.format(self.knowledge)


person = Person('Simone', 1997)
print(person)

student = Student('Giovanni', 1995, 25)
print(student)
