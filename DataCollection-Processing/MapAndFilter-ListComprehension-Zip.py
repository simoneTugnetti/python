# Simone Tugnetti


# Map and Filter

def triple(value):
    return value * 3


# La funzione map() prende come argomento una funzione per eseguire operazioni,
# ed una lista al quale eseguire tale operazione
def triple_map(a_list):
    return map(triple, a_list)


def quadruple_map(a_list):
    return map(lambda val: val * 4, a_list)


# La funzione filter() prende come argomenti un booleano ed una lista, nel caso il primo sia True, aggiunge l'elemento
# controllato all'interno della lista passata ad una nuova lista, nel caso sia False, continua il ciclo
def filter_list(a_list):
    return filter(lambda val: val > 4, a_list)


list1 = [2, 5, 9]
print(list(triple_map(list1)))
print(list(quadruple_map(list1)))

lista = ['GIOVANNI', 'PAOLO', 'GERRY']
print(list(map(lambda x: x.lower(), lista)))

print(list(filter_list(list1)))

print()


# -----------------------------------


# List Comprehensions

list1 = [4, 7, 9]

# Crea una nuova lista prendendo ogni singolo valore di list1, eseguendo un operazione su di esso per poi
# aggiungerlo alla lista, analogo a map()
newList = [value * 2 for value in list1]

# Analogo a filter()
newListFilter = [value for value in list1 if value < 8]

print(newList)
print(newListFilter)

print()


# ----------------------------------


# Zip

L1 = [1, 2, 3]
L2 = [4, 5, 6]

# Viene creata una lista di tuple avente per ognuna i valori contenuti nella stessa posizione delle liste passate
# come parametro
L3 = list(zip(L1, L2))
print(L3)

L4 = []
for (x1, x2) in L3:
    L4.append(x1 + x2)

print(L4)

L4 = [x1 + x2 for (x1, x2) in list(zip(L1, L2))]
print(L4)
