import json

# Simone Tugnetti


# Nested Lists -> Lista contenente liste oltre che valori

nestedLists = [['a', 'b', 'c'], ['d', 'e', 'f'], ['g', 'h']]

print(nestedLists)

nestedLists.append(['i'])
nestedLists[-1].append('j')

print(nestedLists[0][-1])  # Questo è possibile perchè alla posizione 0 è presente una lista

nestedLists[0][-1] = 3

for item in nestedLists:
    print(item)

# Nested Iteration -> Il primo ciclo controlla la lista iniziale ed il secondo la sottolista
for item in nestedLists:
    for subItem in item:
        print(subItem)

# Una Nested List può contenere anche diversi tipi, come interi, stringhe, tuple, dictionary, ecc...
nestedLists2 = ['a', {'b': 2, 'c': 3}, 12, ('d', 'e', 98)]

nestedLists3 = [1, 2, [2.5, 3], [4, 4.5, 6]]

print()

# Verifico che l'elemento all'interno dell lista sia effettivamente una sotto-lista, altrimenti risulterebbe un errore
for x in nestedLists3:
    if type(x) is list:
        for y in x:
            print(y)
    else:
        print(x)

print()

# ---------------------


# Nested Dictionary -> Dictionary contenete dictionaries oltre che valori

info = {
    'name': 'Simone',
    'Surname': 'Tugnetti',
    'age': 23,
    'interest': {
        'music': ['classic', 'rock'],
        'programming': 'Python'
    },
    'cool': True
}

print(info)

interest = info['interest']
print(interest)

programmingLanguage = info['interest']['programming']
print(programmingLanguage)

print()

# -------------------------


# JSON Format and JSON Module

# Le chiavi all'interno della stringa devono essere a doppi apici, così come i valori
stringJson = '{"name": "Simone", "Surname": "Tugnetti", "other": {"language": ["Italian", "English"], "age": 23}}'

d = json.loads(stringJson)  # Converte una stringa in un dictionary usando il JSON Format

print(type(d))
print(d)

# Converte un Dictionary in una Stringa, restituendolo in ordine alfabetico, identato ad ogni sotto-dictionary
stringInfo = json.dumps(info, sort_keys=True, indent=2)

print(stringInfo)
