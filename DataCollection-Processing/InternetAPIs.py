import requests
import json

# Simone Tugnetti


# Internet APIs

# Il modulo request permette di eseguire una chiamata ad un url per poter ricevere il valore restituito ed eseguirne
# diverse operazioni, eseguendo quindi una chiamata REST
page = requests.get('https://www.googleapis.com/books/v1/volumes?q=isbn:9788845290404')
print(type(page))

print(page.text[:100])  # Restitusce i primi 100 caratteri del testo restituito
print(page.url)  # Restituisce l'url utilizzato per effettuare la chiamata

x = page.json()  # Converte il risultato della chiamata in un oggetto riconoscibile in Python

print(x)
print(type(x))

print(json.dumps(x, indent=2))  # Converte il dictionary in String

print()


# E' possibile inserire gli argomenti della query da richiamare tramite dictionary, passandoli come params
isbn = {'q': '9781234567897', 'example': 'book'}
newPage = requests.get('https://www.googleapis.com/books/v1/volumes', params=isbn)

print(newPage.url)

print()
